<?php

abstract class  DataConnector
{


    abstract public function fetchOperatorData();
}
class MySQLDataConnector extends DataConnector
{
    private $servername;    
    private $username;
    private $password;

    public function __construct($ini){
        
        $this->servername = $ini["connection_config"]["servername"];
        $this->username = $ini["connection_config"]["username"];
        $this->password = $ini["connection_config"]["password"];
        $this->dbname = $ini["connection_config"]["dbname"];
    }
    public function fetchOperatorData(){
        // assuming there is a database connection
        $columns = array(
            "id",
            "firstname",
            "lastname");
        $sql = "SELECT ". implode(',', $columns) . " FROM testtable";
        $conn = new mysqli($this->servername,  $this->username,  $this->password,  $this->dbname) or  DIE("DATABASE FAILED TO RESPOND.");
        $result = $conn->query($sql);


       
        $data = array();
        
         // fetching all the data and putting it in an array
         while($row = $result->fetch_assoc()) { $data[] = $row; }
    
         return $data; 
    }
}   

class SqlServerDataConnector extends DataConnector
{

 

    public function __construct($ini){
        
        $this->servername = $ini["connection_config"]["servername"];
        if( $ini["connection_config"]["instance"]!="")
            $this->servername=$this->servername."\\".$ini["connection_config"]["instance"];
        if( $ini["connection_config"]["port"]!="")
            $this->servername=$this->servername.", ".$ini["connection_config"]["port"];


        $this->connectionInfo = array("Database"=>$ini["connection_config"]["dbname"],
                                        "UID"=>$ini["connection_config"]["username"],
                                        "PWD"=>$ini["connection_config"]["password"]);
    }
    public function fetchOperatorData(){
        

        require_once("./fetch.php");
        $sql = file_get_contents("./etc/sqlscript.sql");

        
        $conn = sqlsrv_connect($this->servername,$this->connectionInfo) or DIE(print_r(sqlsrv_errors(),true));
        
        $result = sqlsrv_query($conn, $sql );
        
        $data = array();
        
         // fetching all the data and putting it in an array
         while($row = sqlsrv_fetch_array($result,SQLSRV_FETCH_ASSOC)) { $data[] = $row; }
    
         return $data; 
    }


} 
class  DataConnectorFactory{



    function getDataConnector(){
        $ini = parse_ini_file("./etc/testconfig.ini",TRUE);
        $system = $ini["connection_config"]["system"];
        if($system == "mysql"){
            $this->dc = new MySQLDataConnector($ini);
            return $this->dc;
        }
        elseif($system == "sqlserver"){
            $this->dc = new SqlServerDataConnector($ini);
            return $this->dc;
        }
        else{
            return null;
        }
    }


}

?>
