<?php 
error_reporting(-1);
ini_set('display_errors','On');

?>
<html>
<link rel="stylesheet" type="text/css" href="style.css" media="screen">
    <head>
    <?php $sec = 10; $page = $_SERVER['PHP_SELF'];?>
    <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
    </head>
    <body>
    
        <?php
            require_once("./fetch.php");
            $data = fetchOperatorData();
           
            if ($data != null) {
                //create table header
                echo '<table><thead><tr>';
                $columns = array_keys($data[0]);
                foreach($columns as $column) {
                    echo '<th>'.$column.'</th>';
                }
                echo '</tr></thead><tbody>';
                // output data of each row
                
                foreach($data as $row) {
                    echo '<tr>';
                    foreach(array_keys($row) as $column) {
                        if($column=="ActionTime"){
                            $result = $row[$column]->format('Y-m-d H:i:s');
                            echo '<td>'.$result.'</td>';    
                        }
                        else{
                        echo '<td>'.$row[$column].'</td>';
                        }
                    }
                    echo '</tr>';
                }
                echo '</tbody></table>';
            } else {
                echo "0 results";
            }
        ?>

    </body>
</html>