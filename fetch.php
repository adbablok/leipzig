<?php

require_once("./caching.php");
require_once("./data_connector.php");




$cache = new Sabre_Cache_APC();


    function fetchOperatorData(){
        global $cache;
        global $columns;


        $key = 'operatorData';

        // check if the data is not in the cache already
        if (!$data = $cache->fetch($key)) {
           // there was no cache version, we are fetching fresh data
           $dc = new DataConnectorFactory();
           $data = $dc->getDataConnector()->fetchOperatorData();
           // Storing the data in the cache for 10 seconds
           $cache->store($key,$data,10);
           
        }
        return $data;
      
        
    }


?>